<?php

require_once 'access.php';

$db = new DB(SQL_HOST, SQL_NAME, SQL_PW, SQL_DB);

class DB extends mysqli
{
	private $success;

	public $coreParticipant = "nzgojl_rock.Participant", $csDB = "nzgojl_casesolversevents";

	public function __construct($host, $name, $pw, $db) {

		@parent::__construct($host, $name, $pw, $db);
		$this->success = true;
		if ($this->connect_errno) {
			error_log(SQL_ERR.": " . $this->connect_errno);
		}


		$this -> set_charset("utf8");

	}

	public function getSuccess() {
		return $this -> success;
	}

	public function commitOnSuccess() {
	    if($this -> getSuccess()) {
            $this -> db_query("COMMIT;");
            return true;
        }

	    return false;
    }

    public function db_query($cmd) { //updatekor 0-t ad vissza (stackoverflow szerint)
		$result = $this->query($cmd);
		if(!$result) {
			$this -> success = false;
			$this->writeError($cmd);
			return -1;
		}
		return $this->insert_id;
	}
	
	
	public function db_select($query) {
	
		
		$row = array();
		if ($result = $this->query($query)) {
	
	    	while ($row1 = $result->fetch_assoc()) {
	    		$newrow = $row1;
	    		
	    		/*
	    		//NUM
	    		foreach ($row1 as $key => $value) {
	    			$newrow[] = $value;
				}
				*/
	    		$row[] = $newrow;
	    	}
		
		} else {
		    $this->writeError($query);
        }
	

		/*
		$result = $this->query($query);
		if (!$result) {
			error_log("SQL Errormessage: ".$this->error);
		}
		$row = $result->fetch_all(MYSQL_BOTH);
		$result->free();
		*/

		return $row;
	}
	
	public function strToDB($string)
	{
		return mysqli_real_escape_string($this, $string);//htmlspecialchars($string);
	}

	public static function makeValuesEscapeable(&$arr) { //az eredeti tömböt módosítja
		global $db;

		foreach ($arr as &$value) {
			$value = $db -> strToDB($value);
		}
	}

	public static function formattingValues(&$arr) {
		$sqlStr = "";
		DB::makeValuesEscapeable($arr);
		foreach ($arr as $key => $value) {
			$sqlStr .= $key."='".$value."',";
		}
		return substr($sqlStr, 0, -1)." ";
	}

	private function writeError($cmd) {
        error_log("SQL Errormessage: ".$this->error."\nQuery: ".$cmd);
    }

}