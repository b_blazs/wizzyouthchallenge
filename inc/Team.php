<?php
require_once "DB.php";

class Team {
    private $id, $name, $uploadDate, $members, $registerDate;
    private $db;

    /**
     * Team constructor.
     * @param $id
     * @param $name
     * @param $uploadDate
     * @param $registerDate
     * @param $members
     */
    public function __construct(int $id = 0, string $name = "", string $uploadDate = "", string $registerDate = "", Members $members = null) {
        global $db;
        $this -> db = $db;
        $this -> id = $id;
        $this -> name = $name;
        $this -> uploadDate = $uploadDate;
        $this -> members = $members;
        $this -> registerDate = $registerDate;
    }

    public static function init($id) {
        global $db;
        $id = $db -> strToDB($id);
        $res = $db -> db_select("
            SELECT 
                t.id team_id, t.name team_name, COALESCE(t.upload_date, '') upload_date, t.register_date
            FROM team t
            WHERE t.id = ".$id.";
        ");

        /*
        echo "TEAM INIT:


            SELECT 
                t.id team_id, t.name team_name, t.upload_date, t.register_date
            FROM team t
            WHERE t.id = ".$id.";
        ";

        var_dump($res);
        */

        if(count($res) == 0) {
            return null;
        }

        $res = $res[0];

        $members_res = $db -> db_select("            
            SELECT 
                m.id, m.name, m.birth_date, m.country, m.degree, m.university, m.grad_year
            FROM team_member tm
            JOIN member m ON m.id = tm.member_id
            WHERE tm.team_id = ".$id.";
        ");


        $members = new Members();

        for($i = 0; $i < count($members_res); ++$i) {
            $members[] = Member::init($members_res[$i]);
        }

        //$members[] = new Member(1, "leaderName", "2017", "contry", "degree", "uni", 2007);


        return new Team($id, $res["team_name"], $res["upload_date"], $res["register_date"], $members);
    }

    public function addMember(Member $member) {
        $this -> members[] = $member;
    }

    /**
     * @return mixed
     */
    public function getMembers()
    {
        return $this->members;
    }

    public function isAnyMember() {
        return count($this -> members) != 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }




    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $uploadDate
     */
    public function setUploadDate($uploadDate)
    {
        $this->uploadDate = $uploadDate;
    }







    public function canUpload() {
        return $this->uploadDate == "" || strtotime($this->uploadDate)+UPLOAD_EXTRA_SECS>strtotime("now");
    }

    public function isFirstUpload() {
        return $this->uploadDate == "";
    }

    public function uploadFile($file) {
        //TODO: upload class include
        //mi az anyád az az include?
    }

    public function create() {
        $this -> db -> autocommit(false);

        $name = $this -> db -> strToDB($this->name);
        $this -> id = $this -> db -> db_query("
            INSERT INTO team SET name = '".$name."';
        ");

        if(!$this -> db -> commitOnSuccess()) {
            $this -> id = 0;
            throw new Exception("Can't create team right now.");
        }

        $this -> db -> autocommit(true);
    }

    /**
     * This function updates class data (include the members) in DB.
     * It's NOT save removing members but save the new ones.
     * That's because we don't want to remove members.
     * @throws Exception Case of unsuccessful save to DB
     */
    public function update() {
        $name = $this -> db -> strToDB($this->name);
        $uploadDateSQL = $this -> uploadDate == "" ? "NULL" : "'".$this -> uploadDate."'";
        $this -> db -> db_query("
            UPDATE team SET name = '".$name."', upload_date = ".$uploadDateSQL."
            WHERE id = ".$this -> id."
            ;
        ");

        $this -> db -> autocommit(false);
        $isMembersExist = false;
        $sqlStr = "INSERT IGNORE INTO team_member (team_id, member_id) VALUES ";
        $values = "";
        for($i=0; $i < count($this -> members); ++$i) {
            $member = $this -> members[$i];
            $values .= "(".$this->id.", ".$member->getMemberID()."),";
            $isMembersExist = true;
        }
        if($isMembersExist) {
            $values = substr($values, 0, -1);
            $sqlStr .= $values;
            $this -> db -> db_query($sqlStr);
        }

        if(!$this -> db -> commitOnSuccess()) {
            $this -> id = 0;
            throw new Exception("Can't add members to Team right now.");
        }

        $this -> db -> autocommit(true);

    }

    public function del() {
        $this -> db -> db_query("
          DELETE tm, m  FROM team_member tm
            JOIN member m  ON m.id=tm.member_id
            JOIN team t ON t.id = tm.team_id
          WHERE t.id = ".$this -> id."
            ;
        ");

        $this -> db -> db_query("
          DELETE lm, l FROM team t
            JOIN leader l ON l.team_id = t.id
            JOIN member lm ON lm.id = l.member_id
          WHERE t.id = ".$this -> id."
            ;
        ");


        $this -> db -> db_query("
            DELETE FROM team
            WHERE id = ".$this -> id."
            ;
        ");

        $this -> __construct();
    }


    public static function isNameTaken($name) {
        global $db;
        $name = $db -> strToDB($name);
        $res = $db -> db_select("
            SELECT 
               id
            FROM team
            WHERE name = '".$name."';
        ");
        return count($res) != 0;
    }
}