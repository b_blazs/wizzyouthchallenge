<?php
/**
 * Created by IntelliJ IDEA.
 * User: gaboristvanfi
 * Date: 2018. 01. 11.
 * Time: 12:03
 */
class Uploader {
    private $files, $success, $errorStatus, $errorMessage, $dictionary;
    private $OVERWRITING, $ALLOWED_TYPES, $PATH, $MIN_AMOUNT_OF_FILES, $MAX_AMOUNT_OF_FILES, $MIN_SIZE_PER_FILE, $MAX_SIZE_PER_FILE;

    /**
     * Uploader constructor.
     * @param bool $OVERWRITING         Default: false. In case of false we don't overwrite the existing files. In case of true we do.
     * @param array $ALLOWED_TYPES      Array of the allowed file extensions' string. In case of empty there is no limitation.
     * @param string $PATH              File's path in the filesystem to save. In case of empty the files will be in the same dir as main program.
     * @param int $MIN_AMOUNT_OF_FILES            Minimum amount of files. In case of empty there is no limitation.
     * @param int $MAX_AMOUNT_OF_FILES            Maximum amount of files. In case of empty there is no limitation.
     * @param int $MIN_SIZE_PER_FILE        Minimum kB of size / file. In case of empty there is no limitation.
     * @param int $MAX_SIZE_PER_FILE        Maximum kB of size / file. In case of empty there is no limitation.
     */
    public function __construct($OVERWRITING=false, $ALLOWED_TYPES = array(), $PATH = "", $MIN_AMOUNT_OF_FILES = 0, $MAX_AMOUNT_OF_FILES = 0, $MIN_SIZE_PER_FILE = 0, $MAX_SIZE_PER_FILE = 0)
    {
        $this->files = array();
        $this->success = true;
        $this-> errorStatus = 0;
        $this -> errorMessage = "";
        $this->dictionary = array(
            "Successful upload",
            "Error: There was no file to upload",
            "Error: Not allowed file type",
            "Error: The file(s) already exist.",
            "Error: Can't create the file path, server misconfigured.",
            "Error: Could not save the file(s).",
            "Error: Status code: ",
            "Error: File size too small",
            "Error: Maximum upload size exceeded",
            "Error: Not enough file(s) to upload",
            "Error: Too many files selected"
        );

        $this->OVERWRITING = $OVERWRITING;
        $this->ALLOWED_TYPES = $ALLOWED_TYPES;

        $this->PATH = $PATH;
        $this->MIN_AMOUNT_OF_FILES = $MIN_AMOUNT_OF_FILES;
        $this->MAX_AMOUNT_OF_FILES = $MAX_AMOUNT_OF_FILES;
        $this->MIN_SIZE_PER_FILE = $MIN_SIZE_PER_FILE*1024; //kB to b
        $this->MAX_SIZE_PER_FILE = $MAX_SIZE_PER_FILE*1024; //kB to b


        ini_set("max_execution_time", 900);
    }


    /**
     * @param array $files      The $_FILES[_the name attribute_] arrays of array in case of multiple file.
     * @param array $renameTo   The array of file names to rename (without extension)
     */
    public function multipleFiles($files = array(), $renameTo = array()) {
        //ellenőrzés, hogy van-e egyáltalán file

        if(!is_array($files) || count($files) == 0 || !array_key_exists("tmp_name", $files)
            || !is_array($files["tmp_name"]) || count($files["tmp_name"]) == 0
        ) {
            $this->setErrorStatus(1);
            return;
        }

        $countOfFiles = count($files["tmp_name"]);

        if($countOfFiles < $this -> MIN_AMOUNT_OF_FILES) {
            $this->setErrorStatus(9);
            return;
        }

        if($this -> MAX_AMOUNT_OF_FILES > 0 && $countOfFiles > $this -> MAX_AMOUNT_OF_FILES) {
            $this->setErrorStatus(10);
            return;
        }



        $keys = array_keys($files);
        $emptyFile = true;
        for($i=0; $i<count($files["name"]); ++$i) {
            if($files["tmp_name"][$i] == "") {
                continue;
            }
            $emptyFile = false;
            $file = array();
            for($j = 0; $j < count($keys); ++$j) {
                $file[$keys[$j]] = $files[$keys[$j]][$i];
            }
            $this -> singleFile($file, count($renameTo) > $i && $renameTo[$i] != "" ? $renameTo[$i] : "");
        }

        if($emptyFile) {
            $this->setErrorStatus(1);
        }
    }

    /**
     * @param array $file      The $_FILES[_the name attribute_] array in case of single file.
     * @param string $renameTo   The file name to rename
     */
    public function singleFile($file = array(), $renameTo = "") {

        //ellenőrzés, hogy van-e egyáltalán file
        if(count($file) == 0 || !array_key_exists("tmp_name", $file) || $file["tmp_name"] == "") {
            $this->setErrorStatus(1);
            return;
        }

        if($this->MIN_SIZE_PER_FILE > $file["size"]) {
            $this->setErrorStatus(7);
            return;
        }

        if($this->MAX_SIZE_PER_FILE > 0 && $file["size"] > $this->MAX_SIZE_PER_FILE) {
            $this->setErrorStatus(8);
            return;
        }

        if(!preg_match('/('.$this -> allowedTypesToString("|").')$/i', $file["name"]))
        {
            $this->setErrorStatus(2);
            return;
        }

        $explodeArr = explode(".", $file["name"]);
        $fileExtension = end($explodeArr);

        if($renameTo != "") {
            $file["name"] = $renameTo.".".$fileExtension;
        }

        if($this -> isFileExist($file["name"])) {
            $this->setErrorStatus(3);
            return;
        }

        if(isset($file["error"]) && $file["error"] !== UPLOAD_ERR_OK) {
            $this -> setErrorStatus(6, $file["error"]);
            return;
        }


        $this -> files[] = $file;

    }


    /**
     * @param int $i The index of a file
     * @return string The name of a file
     */
    public function getFileName($i = 0) {
        return $this -> files[$i]["name"];
    }

    /**
     * Get all uploaded file names
     * @return array The array of the uploaded file names
     */
    public function getFilesName() {
        $names = array();
        for($i=0; $i<count($this->files); ++$i) {
            $names[] = $this -> getFileName($i);
        }
        return $names;
    }

    /**
     * @param int $i The index of a file
     * @return string The name of a file path
     */
    public function getFilePath($i=0) {
        return $this->PATH.$this -> files[$i]["name"];
    }

    /**
     * Get all uploaded file names
     * @return array The array of the uploaded file paths
     */
    public function getFilesPath() {
        $paths = array();
        for($i=0; $i<count($this->files); ++$i) {
            $paths[] = $this -> getFilePath($i);
        }
        return $paths;
    }


    /**
     * Saving the upladed files to
     */
    public function saveFiles() {
        if(!$this->success) {
            return;
        }
        $this->createPathIfNotExists();
        for($i = 0; $i < count($this->files); ++$i) {
            if(!move_uploaded_file($this->files[$i]["tmp_name"], $this->PATH . $this->files[$i]["name"])) {
                $this->setErrorStatus(5);
                for($j=$i-1; $j >= 0; $j--) {
                    @unlink($this -> getFilePath($j));
                }
            }
        }
    }

    /**
     * @param string $fileName The name of a file what you want to check
     * @return bool
     */
    private function isFileExist($fileName) {
        if($this->OVERWRITING) {
            return false;
        }

        return is_file($this -> PATH.$fileName);
    }



    /**
     * @return string   Message about the upload status.
     */
    public function getMessage() {

        return $this -> dictionary[$this -> errorStatus].$this->errorMessage;
    }

    /**
     * @return bool
     */
    public function isSuccessful() {
        return $this->success;
    }

    private function createPathIfNotExists() {
        if($this -> PATH != "" && substr($this -> PATH, -1) == "/") {
            $this -> PATH = substr($this -> PATH, 0, -1);
        }

        $folders = explode("/", $this -> PATH);
        $actualPath = "";
        for($i = 0; $i < count($folders); ++$i) {
            $actualPath .= $folders[$i]."/";
            if(!is_dir($actualPath)) {
                if(!@mkdir($actualPath)) {
                    $this->setErrorStatus(4);
                    return;
                }
            }
        }

        /*
        if($this -> PATH != "" && !is_dir(substr($this -> PATH, 0, -1))) { //ha nem létezik a megadott útvonal, akkor megpróbáljuk létrehozni azt.
            echo "PATH: ".$this -> PATH;
            if(!@mkdir($this -> PATH)) {
                $this->setErrorStatus(4);
            }
        }
        */
        $this -> PATH .= "/";
    }

    /**
     * Set the error message according to tstatus code.
     * 1: "Error: There was no file to upload",
     * 2: "Error: Not allowed file type",
     * 3: "Error: The file(s) already exist.",
     * 4: "Error: Can't create the file path, server misconfigured.",
     * 5: "Error: Could not save the file(s)."
     * When you set it the isSuccessful() func will be return false
     * @param int $statusCode   status code
     * @param string $extraInfo if you want to concat string after the status message
     */
    private function setErrorStatus($statusCode, $extraInfo = "") {
        $this -> files = array();
        $this -> success = false;
        $this -> errorStatus = $statusCode;
        $this -> errorMessage = $extraInfo;
    }


    /**
     * It will be return the allowed file types what you define in calling constructor.
     * @param string $separation This string will be the seperator between the types
     * @return string
     */
    private function allowedTypesToString($separation) {
        $str = "";
        for($i=0; $i<count($this -> ALLOWED_TYPES); ++$i) {
            $str .= strtolower($this -> ALLOWED_TYPES[$i]) . $separation;
            $str .= strtoupper($this -> ALLOWED_TYPES[$i]) . $separation;
        }

        if($str != "") {
            $str = substr($str, 0, strlen($str) - strlen($separation));
        }
        return $str;
    }

    /**
     * You can set your own language here. Example:
     * 0: "Successful upload",
     * 1: "Error: There was no file to upload",
     * 2: "Error: Not allowed file type",
     * 3: "Error: The file(s) already exist.",
     * 4: "Error: Can't create the file path, server misconfigured.",
     * 5: "Error: Could not save the file(s)."
     * @param array $dictionary The array of error message
     */
    public function setDictionary($dictionary) {
        $this -> dictionary = $dictionary;
    }


}