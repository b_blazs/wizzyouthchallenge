<?php
require_once "DB.php";

class Members extends \ArrayObject {
    public function offsetSet($key, $val) {
        if ($val instanceof Member) {
            return parent::offsetSet($key, $val);
        }
        throw new \InvalidArgumentException('Value must be a Member');
    }

}

class Member {
    protected $memberID, $name, $birth_date, $country, $degree, $university, $grad_year;
    protected $db;


    public function __construct(int $memberID = 0, string $name = "", string $birth_date = "", string $country = "", string $degree = "", string $university = "", int $grad_year = 0)
    {
        global $db;
        $this->db = $db;
        $this->memberID = $memberID;
        $this->name = $name;
        $this->birth_date = $birth_date;
        $this->country = $country;
        $this->degree = $degree;
        $this->university = $university;
        $this->grad_year = $grad_year;
    }

    public static function init($array) {
        return new Member($array["id"], $array["name"], $array["birth_date"], $array["country"], $array["degree"], $array["university"], $array["grad_year"]);
    }

    public static function initByID(int $memberID) {
        global $db;
        $id = $db -> strToDB($memberID);
        $res = $db -> db_select("
            SELECT 
                id, name, birth_date, country, degree, university, grad_year
            FROM member
            WHERE id = ".$id.";
        ");

        if(count($res) == 0) {
            return null;
        }
        $res = $res[0];

        return self::init($res);
    }

    /**
     * @return int
     */
    public function getMemberID() : int
    {
        return $this->memberID;
    }


    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }



    /**
     * @param string $birth_date
     */
    public function setBirthDate(string $birth_date)
    {
        $this->birth_date = $birth_date;
    }



    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }




    /**
     * @param string $degree
     */
    public function setDegree(string $degree)
    {
        $this->degree = $degree;
    }


    /**
     * @param string $university
     */
    public function setUniversity(string $university)
    {
        $this->university = $university;
    }

    /**
     * @param int $grad_year
     */
    public function setGradYear(int $grad_year)
    {
        $this->grad_year = $grad_year;
    }


    public function update() {
        $data = array(
            "name"          =>  $this -> name,
            "birth_date"    =>  $this -> birth_date,
            "country"       =>  $this -> country,
            "degree"         =>  $this -> degree,
            "university"    =>  $this -> university,
            "grad_year"     =>  $this -> grad_year
        );

        $this -> db -> db_query("
            UPDATE member SET ".DB::formattingValues($data)."
            WHERE id = ".$this -> memberID.";
        ");

        if(!$this -> db -> getSuccess()) {
            throw new Exception("Can't save Member(".$this->memberID.") changes right now. Data: ".DB::formattingValues($data));
        }
    }

    public function create() {
        $data = array(
            "name"          =>  $this -> name,
            "birth_date"    =>  $this -> birth_date,
            "country"       =>  $this -> country,
            "degree"         =>  $this -> degree,
            "university"    =>  $this -> university,
            "grad_year"     =>  $this -> grad_year
        );

        $this -> memberID = $this -> db -> db_query("
            INSERT INTO member SET ".DB::formattingValues($data).";
        ");

        if(!$this -> db -> getSuccess()) {
            $this -> memberID = 0;
            throw new Exception("Can't create member right now.");
        }
    }

    public function del() {
        $this -> db -> db_query("
            DELETE FROM member where id = ".$this -> memberID.";
        ");

        if($this -> db -> getSuccess()) {
            $this->__construct();
        } else {
            throw new Exception("Can't delete member right now.");
        }
    }
    


}