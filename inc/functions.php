<?php

/**
 * Created by IntelliJ IDEA.
 * User: gaboristvanfi
 * Date: 2018. 01. 14.
 * Time: 18:56
 */

const SITEURL = "wizzyouthchallenge.com";

const ALLOWED_TYPES = ["pdf"];
$FILENAME = "FILENAME";
const UPLOAD_PATH = "uploads/";
const UPLOAD_EXTRA_SECS = 300;
const LUNCH_TIME = "25/03/2018 23:59:59"; // d/m/Y H:i:s GMT
const DEADLINE = "16/04/2018 23:59:59";



const MAIL_HOST = 'dfvbin.loginssl.com';
const MAIL_PORT = '587';
const MAIL_NAME = 'wizzair@case-solvers.com';
const MAIL_PW = 'wizzair!PW';

const MAIL_SENDER = "info@wizzyouthchallenge.com";

function sendMail(string $to, string $subject, string $body, bool $onlyText = false) : bool
{

    $headers = array(
        'From' => MAIL_SENDER,
        'To' => $to,
        'Return-Path' => MAIL_SENDER,
        'Subject' => $subject,
        'Content-Type' => 'text/html; charset=UTF-8'
    );

    $crlf = "\n";


    $mime_params = array(
        'text_encoding' => '7bit',
        'text_charset' => 'UTF-8',
        'html_charset' => 'UTF-8',
        'head_charset' => 'UTF-8'
    );

    $mime = new Mail_mime($crlf);


    if($onlyText) {
        $mime->setTXTBody($body);
    } else {
        $mime->setHTMLBody($body);
    }

    $body = $mime->get($mime_params);
    $headers = $mime->headers($headers);


    $params['host'] = MAIL_HOST;
    $params['port'] = MAIL_PORT;
    $params['auth'] = 'PLAIN';
    $params['username'] = MAIL_NAME;
    $params['password'] = MAIL_PW;

    $mail_object = Mail::factory('smtp', $params);


    return $mail_object->send($to, $headers, $body);
}

function MAIL_NEWPASS($leaderName, $pass, $link="") {
    return <<<EOT

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0069)file:///D:/DOWNLOAD/CS_Int_20171105/CS_Internal_Newsletter_October_V1 -->
<html xmlns="http://www.w3.org/1999/xhtml" class="fa-events-icons-ready">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Wizz Youth Challenge</title>
	<style type="text/css">
		body {
			padding: 0;
			min-width: 100%!important;
			font-family: 'Roboto', sans-serif;
			font-size: 16px;
		}

		table {
			border-collapse: collapse;
		}

		p {
			margin: 0px;
			padding: 0px;
			border: none;
		}

		a {
			text-decoration: none;
			font-weight: bold;
			color: #3b5eab;
		}

		a:hover {
			font-weight: bold;
			color: #5781de;
		}

		.title {
			max-width: 600px;
			top: 0;
			right: 0;
			font-size: 18px;
			line-height: 125%;
			background-color: #06038d;
			border-bottom: 0px solid #CCCCCC;
		}

		.title_left {
			color: #FFFFFF;
			font-family: Open Sans, Helvetica;
			font-weight: bold;
			float: left;
			padding-left: 30px;
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.title_right {
			color: #EEEEEE;
			font-family: Open Sans, Helvetica;
			float: right;
			text-align: right;
			padding-right: 30px;
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.head {
			max-width: 600px;
			display: table-cell;
			border-collapse: collapse;
			border-spacing: 0 !important;
			padding: 0px;
			line-height: 0px;
			margin: 0px;
		}

		.intro_1 {
			text-align: justify;
			font-style: italic;
			font-size: 14px;
			padding-bottom: 10px;
		}

		.intro_2 {
			text-align: justify;
			font-style: italic;
			font-size: 14px;
			padding-bottom: 40px;
		}

		.content {
			width: 100%;
			max-width: 600px;
			background-color: #FFF;
		}

		.innerpadding {
			padding: 5% 5% 0% 5%;
		}

		.innerpadding2 {
			padding: 0% 5% 5% 5%;
		}

		.borderbottom {
			border-bottom: 0px solid #f2eeed;
		}

		.h2 {
			padding: 0 0 0px 0;
			font-size: 24px;
			line-height: 28px;
			color: #274f7d;
			font-family: Open Sans, Helvetica;
		}

		.content {
			padding: 0 0 5px 0;
			line-height: 24px;
			text-align: justify;
		}

		.bodycopy {
			font-size: 18px;
			line-height: 24px;
		}

		.footer {
			max-width: 600px;
			background-color: #274f7d;
			color: #FFF !important;
			height: 30px;
		}

		.col_left {
			float: left;
			padding-top: 30px;
			padding-left: 0px;
		}

		.col_right {
			float: left;
			padding-top: 30px;
		}

		.news_title {
			color: #274f7d;
			font-weight: bold;
			text-align: justify;
			font-size: 18px;
		}

		.news_body {
			max-width: 540px;
			text-align: justify;
			font-size: 14px;
			font-style: italic;
		}

		ul {
			max-width: 510px;
			text-align: justify;
			font-size: 14px;
		}

		.footer {
			font-size: 12px;
			font-style: italic;
		}

		.footer_left {
			float: left;
			padding: 10px 0 10px 30px;
		}

		.footer_right {
			float: right;
			text-align: right;
			padding: 10px 30px 10px 0;
		}
	</style>

</head>

<body yahoo="yahoo" bgcolor="#5d5d5d">
	<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
		<tbody>
			<tr>
				<td>
					<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
						<tbody>
							<tr class="title">
								<td class="title_left" colspan="3"> WIZZ Youth Challenge</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
		<tbody>
			<tr>
				<td>


					<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
						<tbody>
							<tr>
								<td>
									<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
										<tbody>
											<tr>
												<td class="innerpadding2 borderbottom" colspan="6">
													<table class="bodycopy" width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr class="content">
																<td class="col_right" colspan="3">
																	<p class="news_body">Dear $leaderName,</p><br>
																	<p class="news_body">Here is your new password: $pass</p>

																	<p class="news_body" style="margin-top:30px;">
																		Best regards,</p>
																	<p class="news_body">
																		The Wizz Youth Challenge Team	
																	</p>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
						<tbody>
							<tr>
								<td>
									<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
										<tbody>
											<tr class="title">
												<td class="title_left" style="font-size:50%" colspan="3"> If you need any help, please contact our support: <a style="color:white" href="mailto:info@wizzyouthchallenge.com">info@wizzyouthchallenge.com</a></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>


EOT;
}

function MAIL_FORGOTPASS($leaderName, $link) {
    return <<<EOT
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0069)file:///D:/DOWNLOAD/CS_Int_20171105/CS_Internal_Newsletter_October_V1 -->
<html xmlns="http://www.w3.org/1999/xhtml" class="fa-events-icons-ready">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Wizz Youth Challenge</title>
	<style type="text/css">
		body {
			padding: 0;
			min-width: 100%!important;
			font-family: 'Roboto', sans-serif;
			font-size: 16px;
		}

		table {
			border-collapse: collapse;
		}

		p {
			margin: 0px;
			padding: 0px;
			border: none;
		}

		a {
			text-decoration: none;
			font-weight: bold;
			color: #3b5eab;
		}

		a:hover {
			font-weight: bold;
			color: #5781de;
		}

		.title {
			max-width: 600px;
			top: 0;
			right: 0;
			font-size: 18px;
			line-height: 125%;
			background-color: #06038d;
			border-bottom: 0px solid #CCCCCC;
		}

		.title_left {
			color: #FFFFFF;
			font-family: Open Sans, Helvetica;
			font-weight: bold;
			float: left;
			padding-left: 30px;
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.title_right {
			color: #EEEEEE;
			font-family: Open Sans, Helvetica;
			float: right;
			text-align: right;
			padding-right: 30px;
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.head {
			max-width: 600px;
			display: table-cell;
			border-collapse: collapse;
			border-spacing: 0 !important;
			padding: 0px;
			line-height: 0px;
			margin: 0px;
		}

		.intro_1 {
			text-align: justify;
			font-style: italic;
			font-size: 14px;
			padding-bottom: 10px;
		}

		.intro_2 {
			text-align: justify;
			font-style: italic;
			font-size: 14px;
			padding-bottom: 40px;
		}

		.content {
			width: 100%;
			max-width: 600px;
			background-color: #FFF;
		}

		.innerpadding {
			padding: 5% 5% 0% 5%;
		}

		.innerpadding2 {
			padding: 0% 5% 5% 5%;
		}

		.borderbottom {
			border-bottom: 0px solid #f2eeed;
		}

		.h2 {
			padding: 0 0 0px 0;
			font-size: 24px;
			line-height: 28px;
			color: #274f7d;
			font-family: Open Sans, Helvetica;
		}

		.content {
			padding: 0 0 5px 0;
			line-height: 24px;
			text-align: justify;
		}

		.bodycopy {
			font-size: 18px;
			line-height: 24px;
		}

		.footer {
			max-width: 600px;
			background-color: #274f7d;
			color: #FFF !important;
			height: 30px;
		}

		.col_left {
			float: left;
			padding-top: 30px;
			padding-left: 0px;
		}

		.col_right {
			float: left;
			padding-top: 30px;
		}

		.news_title {
			color: #274f7d;
			font-weight: bold;
			text-align: justify;
			font-size: 18px;
		}

		.news_body {
			max-width: 540px;
			text-align: justify;
			font-size: 14px;
			font-style: italic;
		}

		ul {
			max-width: 510px;
			text-align: justify;
			font-size: 14px;
		}

		.footer {
			font-size: 12px;
			font-style: italic;
		}

		.footer_left {
			float: left;
			padding: 10px 0 10px 30px;
		}

		.footer_right {
			float: right;
			text-align: right;
			padding: 10px 30px 10px 0;
		}
	</style>

</head>

<body yahoo="yahoo" bgcolor="#5d5d5d">
	<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
		<tbody>
			<tr>
				<td>
					<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
						<tbody>
							<tr class="title">
								<td class="title_left" colspan="3"> WIZZ Youth Challenge</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
		<tbody>
			<tr>
				<td>


					<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
						<tbody>
							<tr>
								<td>
									<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
										<tbody>
											<tr>
												<td class="innerpadding2 borderbottom" colspan="6">
													<table class="bodycopy" width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr class="content">
																<td class="col_right" colspan="3">
																	<p class="news_body">Dear $leaderName,</p><br>
																	<p class="news_body">It looks like you forgot your password.</p><br>
																	<p class="news_body">To get a new one, please click to this link: <a href="$link" target="_blank">$link</a></p>
																	<p class="news_body" style="margin-top:20px;font-weight:bold;">If you did not initiate this process, please ignore this email!</p>
																	<p class="news_body" style="margin-top:30px;">
																		Thank you!</p>
																		<p class="news_body">
																			The Wizz Youth Challenge Team	
																		</p>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
										<tbody>
											<tr>
												<td>
													<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
														<tbody>
															<tr class="title">
																<td class="title_left" style="font-size:50%" colspan="3"> If you need any help, please contact our support: <a style="color:white" href="mailto:info@wizzyouthchallenge.com">info@wizzyouthchallenge.com</a></td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>


EOT;

}
function MAIL_CONFIRM($leaderName, $link) {
    return <<<EOT
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0069)file:///D:/DOWNLOAD/CS_Int_20171105/CS_Internal_Newsletter_October_V1 -->
<html xmlns="http://www.w3.org/1999/xhtml" class="fa-events-icons-ready">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Wizz Youth Challenge</title>
	<style type="text/css">
		body {
			padding: 0;
			min-width: 100%!important;
			font-family: 'Roboto', sans-serif;
			font-size: 16px;
		}

		table {
			border-collapse: collapse;
		}

		p {
			margin: 0px;
			padding: 0px;
			border: none;
		}

		a {
			text-decoration: none;
			font-weight: bold;
			color: #3b5eab;
		}

		a:hover {
			font-weight: bold;
			color: #5781de;
		}

		.title {
			max-width: 600px;
			top: 0;
			right: 0;
			font-size: 18px;
			line-height: 125%;
			background-color: #06038d;
			border-bottom: 0px solid #CCCCCC;
		}

		.title_left {
			color: #FFFFFF;
			font-family: Open Sans, Helvetica;
			font-weight: bold;
			float: left;
			padding-left: 30px;
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.title_right {
			color: #EEEEEE;
			font-family: Open Sans, Helvetica;
			float: right;
			text-align: right;
			padding-right: 30px;
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.head {
			max-width: 600px;
			display: table-cell;
			border-collapse: collapse;
			border-spacing: 0 !important;
			padding: 0px;
			line-height: 0px;
			margin: 0px;
		}

		.intro_1 {
			text-align: justify;
			font-style: italic;
			font-size: 14px;
			padding-bottom: 10px;
		}

		.intro_2 {
			text-align: justify;
			font-style: italic;
			font-size: 14px;
			padding-bottom: 40px;
		}

		.content {
			width: 100%;
			max-width: 600px;
			background-color: #FFF;
		}

		.innerpadding {
			padding: 5% 5% 0% 5%;
		}

		.innerpadding2 {
			padding: 0% 5% 5% 5%;
		}

		.borderbottom {
			border-bottom: 0px solid #f2eeed;
		}

		.h2 {
			padding: 0 0 0px 0;
			font-size: 24px;
			line-height: 28px;
			color: #274f7d;
			font-family: Open Sans, Helvetica;
		}

		.content {
			padding: 0 0 5px 0;
			line-height: 24px;
			text-align: justify;
		}

		.bodycopy {
			font-size: 18px;
			line-height: 24px;
		}

		.footer {
			max-width: 600px;
			background-color: #274f7d;
			color: #FFF !important;
			height: 30px;
		}

		.col_left {
			float: left;
			padding-top: 30px;
			padding-left: 0px;
		}

		.col_right {
			float: left;
			padding-top: 30px;
		}

		.news_title {
			color: #274f7d;
			font-weight: bold;
			text-align: justify;
			font-size: 18px;
		}

		.news_body {
			max-width: 540px;
			text-align: justify;
			font-size: 14px;
			font-style: italic;
		}

		ul {
			max-width: 510px;
			text-align: justify;
			font-size: 14px;
		}

		.footer {
			font-size: 12px;
			font-style: italic;
		}

		.footer_left {
			float: left;
			padding: 10px 0 10px 30px;
		}

		.footer_right {
			float: right;
			text-align: right;
			padding: 10px 30px 10px 0;
		}
	</style>

</head>

<body bgcolor="#5d5d5d">
	<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
		<tbody>
			<tr>
				<td>
					<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
						<tbody>
							<tr class="title">
								<td class="title_left" colspan="3"> WIZZ Youth Challenge</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
		<tbody>
			<tr>
				<td>


					<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
						<tbody>
							<tr>
								<td>
									<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
										<tbody>
											<tr>
												<td class="innerpadding2 borderbottom" colspan="6">
													<table class="bodycopy" width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr class="content">
																<td class="col_right" colspan="3">
																	<p class="news_body">Dear $leaderName,</p><br>
																	<p class="news_body">Thank you for your team application to the Wizz Youth Challenge. After the application period is over, on 26 March, we will send you the case study and all other necessary documents for the challenge. Please do not forget to send us a paper version of the <a href="https://wizzyouthchallenge.com/frontend/assets/files/reg-form.pdf">registration form</a> in order to finalize your application.</p>
																	<p class="news_body" style="margin-top:20px;">To Confirm your email, please click to this link: <a href="$link" target="_blank">$link</a></p>
																	<p class="news_body" style="margin-top:30px;">
																		Thank you and good luck!</p>
																		<p class="news_body">
																			The Wizz Youth Challenge Team	
																		</p>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<table width="100%" bgcolor="#5d5d5d" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse !important;">
						<tbody>
							<tr>
								<td>
									<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
										<tbody>
											<tr class="title">
												<td class="title_left" style="font-size:50%" colspan="3"> If you need any help, please contact our support: <a style="color:white" href="mailto:info@wizzyozthchallenge.com">info@wizzyouthchallenge.com</a></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>


EOT;
}








function response(bool $success, string $msg = "", string $nextURL = "") {
    exit(
    json_encode(
        array(
            "success"       => $success,
            "msg"           => $msg,
            "url"           => $nextURL
        )
    )
    );
}