<?php

require_once 'functions.php';
require_once 'DB.php';
require_once 'Member.php';
require_once 'Team.php';

require_once 'Mail.php';
require_once 'Mail/mime.php';

class Leader extends Member {
    const FROMMAIL = "info@wizz.case-solvers.com";
	private $id, $email, $token, $pass, $reset_pass_hash, $actived, $team;


	public function __construct(int $id = 0, string $email = "", string $token = "", string $pass = "", int $actived = 0, Team $team = null, int $memberID = 0, string $name = "", string $birth_date = "", string $country = "", string $degree = "", string $university = "", int $grad_year = 0) {
        $this -> id = $id;
        $this -> email = $email;
        $this -> token = $token;
        $this -> pass = $pass;
        $this -> actived = $actived;
        $this -> team = $team == null ? new Team() : $team;
        parent::__construct($memberID, $name, $birth_date, $country, $degree, $university, $grad_year);
        $this -> saveToken();
        /*
        echo "Team in Leader constructor:
        ";
        var_dump($this -> team);
        */
	}

	private static function getDataForInit(string $whereParams) {
	    global $db;
        $res = $db -> db_select("
            SELECT 
                l.id leader_id, l.email, l.team_id, l.email, l.token, l.pass, l.actived=1 actived,
                m.id member_id, m.name, m.birth_date, m.country, m.degree, m.university, m.grad_year
            FROM leader l
            JOIN member m ON m.id = l.member_id
            WHERE ".$whereParams."
            LIMIT 1;
        ");

        //var_dump($res);

        if(count($res) == 0) {
            return null;
        }
	    return $res[0];
    }

    public static function init($array) {
	    return new Leader(
            $array["leader_id"],
            $array["email"],
            $array["token"],
            $array["pass"],
            $array["actived"],
            Team::init($array["team_id"]),
            $array["member_id"],
            $array["name"],
            $array["birth_date"],
            $array["country"],
            $array["degree"],
            $array["university"],
            $array["grad_year"]
        );
    }


	public static function initByID(int $leaderID) {
        global $db;
        $leaderID = $db -> strToDB($leaderID);
        $res = self::getDataForInit("
            l.id = ".$leaderID."
        ");

        if($res == null) {
            return new Leader();
        }
        return self::init($res);
    }

    public static function initByEmail(string $email) : Leader {
        global $db;
        $email = $db -> strToDB($email);
        $res = self::getDataForInit("
            l.email = '".$email."'
        ");
        if($res == null) {
            return new Leader();
        }
        return self::init($res);
    }

    public static function initByResetHash(int $leaderID, string $hash) : Leader {
	    global $db;
        $leaderID = $db -> strToDB($leaderID);
        $hash = $db -> strToDB($hash);
        $res = self::getDataForInit("
            l.id = ".$leaderID." AND l.reset_pass_hash = '".$hash."'
        ");
        if($res == null) {
            return new Leader();
        }
        return self::init($res);
    }

    public static function initByActivation(int $leaderID, string $hash) : Leader {
        global $db;
        $leaderID = $db -> strToDB($leaderID);
        $leader = self::initByID($leaderID);
        if($leader -> getActiveHash() != $hash) {
            return new Leader();
        }
        $leader -> setActived(true);
        return $leader;
    }

    public static function initByToken() : Leader {
        global $db;
        $tokenData = self::readToken();
        if(!$tokenData["exist"]) {
            return new Leader();
        }
        $leaderID = $db -> strToDB($tokenData["id"]);
        $token = $db -> strToDB($tokenData["token"]);
        $res = self::getDataForInit("
            l.id = ".$leaderID." AND l.token = '".$token."'
        ");
        if($res == null) {
            return new Leader();
        }
        return self::init($res);
    }

    public static function initByLogin($email, $pass) : Leader {
        global $db;
        $email = $db -> strToDB($email);
        $res = self::getDataForInit("
            l.pass = '".self::getPwHash($pass)."' AND l.email = '".$email."'
        ");
        if($res == null) {
            return new Leader();
        }


        $res["token"] = self::generateToken($res["leader_id"].$res["pass"]);

        return self::init($res);
    }

    public function getTeam() : Team {
	    return $this -> team;
    }

    /**
     * @param Team $team
     */
    public function setTeam(Team $team)
    {
        $this->team = $team;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }






    public function setPass(string $pass) {
	    $this -> pass = Leader::getPwHash($pass);
	    $this -> token = self::generateToken($pass);
	    $this -> reset_pass_hash = "";
    }

	public function changePass(string $newPass) {
		if(!$this -> isLogged()) {
			return false;
		}
		$newPass = self::getPwHash($newPass);
        $this -> token = self::generateToken($this -> id . $this -> pass);
		$this -> db -> db_query("
          UPDATE leader
          SET token = '".$this -> token."', pass = '".$newPass."'
          WHERE id = ".$this -> id.";
        ");
		$this -> saveToken();
		return true;
	}

	public function logout() {
        self::clearToken();
	}


	public function createWithNewTeam(string $teamName) {
        $this -> db -> autocommit(false);
        $this -> team = new Team();
        $this -> team -> setName($teamName);
        $this -> team -> create();
        $this -> token = self::generateToken($this -> memberID.$this -> pass);
        $this -> create();
        $this -> saveToken();
        $this -> db -> autocommit(true);
    }


    public function create() {
        $this -> db -> autocommit(false);
	    parent::create();


	    $leaderObj = array(
	        "team_id"       =>  $this -> team -> getId(),
	        "member_id"     =>  $this -> memberID,
	        "email"         =>  $this -> email,
	        "pass"          =>  $this -> pass,
            "token"         =>  $this -> token
        );
        $this -> id = $this -> db -> db_query("
          INSERT INTO leader
          SET ".DB::formattingValues($leaderObj).";
        ");

        if(!$this -> db -> commitOnSuccess()) {
            $this -> id = 0;
            throw new Exception("Can't create leader right now.");
        }

        $this -> db -> autocommit(true);

    }

    public function update() {
        parent::update();

        $data = array(
            "email"             =>  $this -> email,
            "pass"              =>  $this -> pass,
            "token"             =>  $this -> token,
            "actived"           =>  $this -> actived
        );

        $toSet = DB::formattingValues($data).", reset_pass_hash = ";
        $toSet .= $this -> reset_pass_hash == "" ? "NULL" : "'".$this -> reset_pass_hash."'";

        $this -> db -> db_query("UPDATE leader SET ".$toSet." WHERE id = ".$this -> id.";");

        if(!$this -> db -> getSuccess()) {
            throw new Exception("Can't save Leader(id: ".$this -> id.") changes right now. Data: ".$toSet);
        }

    }

    public function del() {
        parent::del();
        $this -> db -> db_query("DELETE FROM leader WHERE id = ".$this -> id.";");
        if($this -> db -> getSuccess()) {
            $this->__construct();
        } else {
            throw new Exception("Can't delete member right now.");
        }
    }

    public function sendConfirmMail() {
        $link = "http://".SITEURL."/account.php?a=active&id=".$this->id."&hash=".$this->getActiveHash();
        return $this->sendMail(
            "Thank you for your application to the WIZZ Youth Challenge",
            MAIL_CONFIRM($this->getName(), $link)
        );
    }

    /**
     * Throws exception when user doesn't exist.
     * Returns E-mail was successfuly sent.
     * @return bool
     * @throws Exception
     */
    public function sendForgotPassMail() : bool {
        if($this -> email == "") {
            Throw new Exception("This user doesn't exist.");
        }
        $this -> reset_pass_hash = self::generateToken($this->id.time()."forgot");
        $this -> update();
        $link = "http://".SITEURL."/account.php?a=reset&id=".$this->id."&hash=".$this->reset_pass_hash;
        return $this->sendMail(
            "WIZZ Youth Challenge new Password request",
            MAIL_FORGOTPASS($this->getName(), $link));
    }

    /**
     * @return bool
     */
    public function sendNewPassInMail(string $pass) : bool {
        if($this -> email == "") {
            return false;
        }
        return $this -> sendMail(
            "WIZZ Youth Challenge new Password",
            MAIL_NEWPASS($this->getName(), $pass)
            );
    }


    private function sendMail(string $subject, string $body) : bool {
        return sendMail($this->email, $subject, $body);
        $headers = array(
            'From'          => MAIL_SENDER,
            'To'            => $this -> email,
            'Return-Path'   => MAIL_SENDER,
            'Subject'       => $subject,
            'Content-Type'  => 'text/html; charset=UTF-8'
        );

        $crlf = "\n";


        $mime_params = array(
            'text_encoding' => '7bit',
            'text_charset'  => 'UTF-8',
            'html_charset'  => 'UTF-8',
            'head_charset'  => 'UTF-8'
        );

        $mime = new Mail_mime($crlf);


        $mime->setHTMLBody($body);

        $body = $mime->get($mime_params);
        $headers = $mime->headers($headers);


        $params['host'] = MAIL_HOST;
        $params['port'] = MAIL_PORT;
        $params['auth'] = 'PLAIN';
        $params['username'] = MAIL_NAME;
        $params['password'] = MAIL_PW;

        $mail_object = Mail::factory('smtp', $params);


        return $mail_object->send($this -> email, $headers, $body);


        /*
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <'.self::FROMMAIL.'>' . "\r\n";
        //$headers .= 'Cc: myboss@example.com' . "\r\n";





        //return mail($this -> email,$subject,$content,$headers);//, "-r info@wizz.case-solvers.com");
        */
    }

    function getActiveHash() : string {
        return sha1("wizzactivehash".$this->id."activehashwizz");
    }

    /**
     * @param int $actived
     */
    private function setActived($actived)
    {
        $this->actived = $actived;
    }





	public function getID() : int {
		return $this->id;
	}


	public function isLogged() : bool {
		return $this->getID() > 0;
	}

	public function isActived() : bool {
	    return $this -> actived == 1;
    }

    private static function clearToken() {
        setcookie("id", null, -1, '/');
        setcookie("token", null, -1, '/');
    }

	private static function readToken() {
	    return array(
	        "exist" =>  isset($_COOKIE["id"]) && isset($_COOKIE["token"]),
	        "id"    =>  isset($_COOKIE["id"])?$_COOKIE["id"]:"",
            "token"  =>  isset($_COOKIE["token"])?$_COOKIE["token"]:""
        );
    }

    private static function generateToken(string $secret) : string {
        return sha1(time().$secret.time());
    }

	private function saveToken() {
        if(!$this -> isLogged()) {
            self::clearToken();
            return;
        }
		$this->db->db_query("UPDATE leader SET token = '".$this->token."' WHERE id = ".$this->id.";");
        setcookie("id", $this->id, time() + (86400 * 30), "/");
		setcookie("token", $this->token, time() + (86400 * 30), "/");
	}


	private static function getPwHash($pass) : string {
		return sha1($pass);
	}

    public static function isEmailTaken(string $email) : bool {
        global $db;
        $email = $db -> strToDB($email);
        $res = $db -> db_select("
            SELECT 
                id
            FROM leader
            WHERE email = '".$email."';
        ");
        return count($res) != 0;
    }

}