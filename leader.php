<?php
require_once "inc/DB.php";
require_once "inc/Leader.php";
require_once "inc/Team.php";
require_once "inc/functions.php";

header('Content-Type: application/json');


/*
INSERT INTO `team` (`id`, `name`, `upload_date`, `register_date`) VALUES (1, 'tesztTeam', NULL, CURRENT_TIMESTAMP);
    INSERT INTO `member` (`id`, `name`, `birth_date`, `country`, `degree`, `university`, `grad_year`) VALUES (1, 'tesztname', 'birth_date', 'contry', 'degree', 'university', '2200');
    INSERT INTO `leader` (`id`, `team_id`, `member_id`, `email`, `pass`, `token`, `reset_pass_hash`, `actived`) VALUES (NULL, '1', '1', 'teszt@teszt.hu', SHA1('teszt'), '', '', '1');
 */

if(!isset($_GET["action"])) {
	exit(json_encode(array("err" => "Nem érkezett adat.")));
}

$action = $_GET["action"];

$leader = Leader::initByToken();
$success=true;
$msg = "";

if($action == "login") {
    $leader = Leader::initByLogin($_POST["email"], $_POST["password"]);
    $success = $leader->isLogged();
    if($success) {
        response(true, "", $leader -> getTeam() -> isAnyMember() ? "upload.php" : "teamregistration.php");
    } else {
        response(false, "Wrong login data.", "");

    }
} else if($action == "signup") {
    $team_name = $_POST["team_name"];
    $leader_name = $_POST["leader_name"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    //teamName, leaderName, leaderEmail, password

    //check mail, check teamname
    if(Leader::isEmailTaken($email)) {
        response(false, "This e-mail is already taken.");
    }

    if(Team::isNameTaken($team_name)) {
        response(false, "This team name is already taken.");
    }


    $leader = new Leader();


    $leader -> setName($leader_name);
    $leader -> setEmail($email);
    $leader -> setPass($password);
    try {
        $leader -> createWithNewTeam($team_name);
    } catch (Exception $e) {
        error_log("Error on saving created Leader: " . $e->getMessage());
        response(false, "Please try again." );
    }
    response(true, "", $leader -> getTeam() -> isAnyMember() ? "upload.php" : "teamregistration.php");

} else if($action == "forgot") {
    $email = isset($_POST["email"])?$_POST["email"]:"";
    $leader = Leader::initByEmail($email);
    try {
        $success = $leader->sendForgotPassMail();
    } catch (Exception $e) {
        response(false, "This e-mail address is not registered yet.");
    }
    if(!$success) {
        error_log("Couldn't send forgot pass e-mail to ".$email.".");
        response(false, "Please try again.");
    }
    response(true, "Your validation link for new password has been sent to your email. Please check your inbox.");

}

if(!$leader -> isLogged()) {
    response(false, "You are not logged in.");
}

if ($action == "add-members") {

    $leader_name = $_POST["leader_name"];
    $leader_dateofbirth = $_POST["leader_dateofbirth"];
    $leader_country = $_POST["leader_country"];
    $leader_degree = $_POST["leader_degree"];
    $leader_university = $_POST["leader_university"];
    $leader_gradyear = $_POST["leader_gradyear"];
    $leader->setName($leader_name);
    $leader->setBirthDate($leader_dateofbirth);
    $leader->setCountry($leader_country);
    $leader->setDegree($leader_degree);
    $leader->setUniversity($leader_university);
    $leader->setGradYear((int)$leader_gradyear);



    $team = $leader->getTeam();

    $showedCoPilots = (int)$_POST["showedCoPilots"]-1;

    $members_name = $_POST["copilot"];
    $members_dateofbirth = $_POST["dateofbirth"];
    $members_country = $_POST["country"];
    $members_degree = $_POST["degree"];
    $members_university = $_POST["university"];
    $members_gradyear = $_POST["gradyear"];

    /*
    var_dump($members_dateofbirth);
    var_dump($members_country);
    var_dump($members_degree);
    var_dump($members_university);
    var_dump($members_gradyear);
    */

    try {
        $db->autocommit(false);
        for ($i = 0; $i < $showedCoPilots; ++$i) {
            $member = new Member(0,
                $members_name[$i],
                $members_dateofbirth[$i],
                $members_country[$i],
                $members_degree[$i],
                $members_university[$i],
                (int)$members_gradyear[$i]
            );
            $member->create();
            $team->addMember($member);
        }
        $leader->update();
        $team->update();
    } catch (Exception $e) {
        error_log($db->getSuccess()."Error on trying members add to Team: " . $e->getMessage());
        $success = false;
        $msg = "Please try again.";
    }
    if($db->commitOnSuccess()) {
        $db->autocommit(true);
        $leader -> sendConfirmMail();
        $msg = "Thank you for your registration, we will soon be in touch. Please don't forget the paper form.";
    }

}

response($success, $msg, "upload.php");



?>