<?php
require_once "inc/functions.php";
require_once 'inc/Leader.php';


$leader = Leader::initByToken();
if(!$leader -> isLogged()) {
    exit("Session expired. Please refresh the site.");
}
if($leader -> getTeam() -> isAnyMember()) {
    exit("You've already added your team members. Please refresh the site.");
}

$team_name = $leader -> getTeam() -> getName();
$leaderName = $leader -> getName();
?>
<div class="teamreg">
    <div class="row">
        <div class="container nopadding">
            <div class="attention">
                <div class="text">
                    <p>Note: an original copy of the <a class="pdf-href" href="frontend/assets/files/reg-form.pdf" target="_blank">registration form</a> needs to be signed by all team members and sent via post to Wizz Air in order to complete your participation.</p>
                </div>
                <div class="sign"><img src="frontend/assets/img/exclamation.svg"></div>
            </div>
            <div class="teamreg-header">
                <h1>Welcome <?=$team_name?>!</h1>
                <h2>You will need a Pilot and at least 1 Co-pilot to complete your team</h2>
            </div>
            <div id="add-members-form" class="teamreg-form">
                <form name="teamregform">
                    <input type="hidden" id="showedCoPilots" name="showedCoPilots" value="2">
                    <div class="form-group-reg">
                        <p>Pilot</p>
                        <input type="text" name="leader_name" id="teamleadername" tabindex="1" class="form-control-lrg" placeholder="Team Leader" value="<?=$leaderName?>">
                        <input type="text" name="leader_degree" id="degree" tabindex="1" class="form-control-lrg" placeholder="Degree (eg. BA in Finance)">
                        <input type="text" name="leader_university" id="university" tabindex="1" class="form-control-lrg" placeholder="University">
                        <select type="text" name="leader_country" id="unicountry1" tabindex="1" class="form-control-lrg" placeholder="Country of University"></select>
                        <input type="text" name="leader_gradyear" id="gradyear" tabindex="1" class="form-control-sml" placeholder="Graduation year">
                        <input type="text" name="leader_dateofbirth" id="dateofbirth" tabindex="1" class="form-control-sml" placeholder="Date of birth" onfocus="this.placeholder='(yyyy/mm/dd)'" onblur="this.placeholder = 'Date of birth'">

                    </div>
                    <div class="form-group-reg">
                        <p>Co-pilot 1</p>
                        <input type="text" name="copilot[]" tabindex="1" class="form-control-lrg" placeholder="First & Last Name" required>
                        <input type="text" name="degree[]" tabindex="1" class="form-control-lrg" placeholder="Degree (eg. BA in Finance)" required>
                        <input type="text" name="university[]" tabindex="1" class="form-control-lrg" placeholder="University" required>
                        <select type="text" name="country[]" id="unicountry2" tabindex="1" class="form-control-lrg" placeholder="Country of University" required></select>
                        <input type="text" name="gradyear[]" tabindex="1" class="form-control-sml" placeholder="Graduation year" required>
                        <input type="text" name="dateofbirth[]"  tabindex="1" class="form-control-sml" placeholder="Date of birth" required onfocus="this.placeholder='(yyyy/mm/dd)'" onblur="this.placeholder = 'Date of birth'">
                    </div>
                    <div class="form-group-reg hidden" id="co-pilot2">
                        <p>Co-pilot 2</p>
                        <input type="text" name="copilot[]" id="copilot3" tabindex="1" class="form-control-lrg" placeholder="First & Last Name" required>
                        <input type="text" name="degree[]" id="degree3" tabindex="1" class="form-control-lrg" placeholder="Degree (eg. BA in Finance)" required>
                        <input type="text" name="university[]" id="university3" tabindex="1" class="form-control-lrg" placeholder="University" required>
                        <select type="text" name="country[]" id="unicountry3" tabindex="1" class="form-control-lrg" placeholder="Country of University" required></select>
                        <input type="text" name="gradyear[]" id="gradyear3" tabindex="1" class="form-control-sml" placeholder="Graduation year" required>
                        <input type="text" name="dateofbirth[]" id="dateofbirth3" tabindex="1" class="form-control-sml" placeholder="Date of birth" required onfocus="this.placeholder='(yyyy/mm/dd)'" onblur="this.placeholder = 'Date of birth'">
                    </div>
                    <div class="form-group-reg hidden" id="co-pilot3">
                        <p>Co-pilot 3</p>
                        <input type="text" name="copilot[]" id="copilot4" tabindex="1" class="form-control-lrg" placeholder="First & Last Name" required>
                        <input type="text" name="degree[]" id="degree4" tabindex="1" class="form-control-lrg" placeholder="Degree (eg. BA in Finance)" required>
                        <input type="text" name="university[]" id="university4" tabindex="1" class="form-control-lrg" placeholder="University" required>
                        <select type="text" name="country[]" id="unicountry4" tabindex="1" class="form-control-lrg" placeholder="Country of University" required></select>
                        <input type="text" name="gradyear[]" id="gradyear4" tabindex="1" class="form-control-sml" placeholder="Graduation year" required>
                        <input type="text" name="dateofbirth[]" id="dateofbirth4" tabindex="1" class="form-control-sml" placeholder="Date of birth" required onfocus="this.placeholder='(yyyy/mm/dd)'" onblur="this.placeholder = 'Date of birth'">
                    </div>
                    <div class="member" id="add">
                        <a href="" id="addmember-link" ><svg fill="none" height="24" stroke="#afafaf" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><rect height="18" rx="2" ry="2" width="18" x="3" y="3"/><line x1="12" x2="12" y1="8" y2="16"/><line x1="8" x2="16" y1="12" y2="12"/></svg> Add co-pilot</a>
                    </div>
                    <div class="member hidden" id="remove">
                        <a href="" id="removemember-link" ><svg fill="none" height="24" stroke="#afafaf" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><rect height="18" rx="2" ry="2" width="18" x="3" y="3"/><line x1="8" x2="16" y1="12" y2="12"/></svg> Remove co-pilot</a>
                    </div>
                    <div class="check-container">
                        <div class="form-group text">
                            <div class="check">
                                <input type="checkbox" tabindex="3" class="" name="termsnconds" id="termsnconds">
                                <label for="termsnconds" class="signuplabel"> We hereby confirm that all team members have read and understood the <a class="pdf-href" href="frontend/assets/files/terms-conds.pdf" target="_blank">Terms & Conditions</a> of the WIZZ Youth Challenge (“T&C”), especially its provisions on intellectual property rights (Section 9), data protection (Section 10) and limitation of liability (Section 11). By submitting this form all team members accept the terms of said T&C including without limitation to its Sections 9,10, and 11, all of which shall be binding and shall be deemed incorporated herein by way of reference. We agree that this form shall constitute an agreement between all team members and Wizz Air for the transfer of rights / grant of licenses as set forth in the T&C.  By signing this form we grant our consent to Wizz Air to process personal data for the purposes specified in the T&C.</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-reg lg ">
                        <button class="btn regbutton" type="submit" name="reg-submit" id="reg-submit">
                            <img src="frontend/assets/img/takeoff.png"> Let's take off!
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    populateCountries("unicountry1");
    populateCountries("unicountry2");
    populateCountries("unicountry3");
    populateCountries("unicountry4");
</script>    