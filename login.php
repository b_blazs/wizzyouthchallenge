<div class="banner">
    <div class="row">
        <div class="col nopadding">
            <div class="bannerTextContainer">
                <span class="bText bold white wizz">WIZZ YOUTH CHALLENGE</span><br>
                <span class="bText white">SHOW US <span class="bold">YOUR IDEAS</span>,</span><br>
                <span class="bText white">WE SHOW YOU <span class="bold">THE WORLD!</span></span>
            </div>
        </div>

        <div class="col text-center mx-auto">
            <div class="container">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="panelhead left active" id="signup-panelhead">
                                <a href="#" class="active" id="signup-form-link">Sign up</a>
                            </div>
                            <div class="panelhead right" id="login-panelhead">
                                <a href="#"  id="login-form-link">Login</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12 nopadding">
                                <form id="signup-form" name="signup-form" action="" method="post" style="display: block;" novalidate="novalidate">
                                    <div class="form-group">
                                        <input type="text" name="team_name" tabindex="1" class="form-control" placeholder="Team Name" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="leader_name" tabindex="1" class="form-control" placeholder="Team Leader First & Last Name" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" tabindex="1" class="form-control" placeholder="Team Leader's Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <div class="check">
                                            <input type="checkbox" tabindex="3" class="" name="termsnconds" id="termsnconds">
                                            <label for="termsnconds" class="signuplabel"> I have read and agreed to the <a class="pdf-href" href="frontend/assets/files/terms-conds.pdf" target="_blank">terms and conditions</a></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="check">
                                            <input type="checkbox" tabindex="3" class="" name="personaldata" id="personaldata">
                                            <label for="personaldata" class="signuplabel"> I consent to the processing of my personal data</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn formbutton" type="submit" name="signup-submit" id="signup-submit">
                                            I'm up for the challenge! <img src="frontend/assets/img/arrow-right.png" alt="arrow right">
                                        </button>
                                    </div>
                                </form>
                                <form id="login-form" name="login-form" action="" method="post" style="display: none;" novalidate="novalidate">
                                    <div class="form-group">
                                        <input type="text" name="email" tabindex="1" class="form-control" placeholder="Team Leader's Email" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <a href="#" class="forgot">Forgot password</a>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn formbutton" type="submit" name="login-submit" id="login-submit">
                                            Login <img src="frontend/assets/img/login.png" alt="login">
                                        </button>
                                    </div>
                                </form>
                                <form id="reset-form" name="reset-form" action="" method="post" style="display: none;" novalidate="novalidate">
                                    <div class="form-group">
                                        <p class="cancel">< Cancel</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="email" tabindex="1" class="form-control" placeholder="Team Leader's Email" value="">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn formbutton" type="submit" name="reset-submit" id="reset-submit">
                                            Reset
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>