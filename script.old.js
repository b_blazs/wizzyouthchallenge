var showedCoPilots = 2;
var myDropzone = null;

function delete_check(){
    return confirm("Are you sure you want to delete your account?");
}

function confirmAddMembers() {
    return confirm("Are you sure you want to finalize your team? You can't change it after registration.");
}

function formsValidates() {
    $("form[name='reset-form']").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        errorPlacement: function(){
            return false;
        },

        submitHandler: function(form) {
            return true;
            //form.submit();
        }
    });

    $("form[name='login-form']").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 4,
                maxlength: 24
            }
        },
        errorPlacement: function(){
            return false;
        },

        submitHandler: function(form) {
            return true;
            //form.submit();
        }
    });

    $("form[name='signup-form']").validate({
        rules: {
            team_name: "required",
            leader_name: "required",
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 4,
                maxlength: 24
            },
            termsnconds: "required",
            personaldata: "required"
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function(form) {
            return true;
            //form.submit();
        }
    });
    $("form[name='teamregform']").validate({
        ignore: ".ignore",
        rules: {
            leader_name: "required",
            leader_degree: "required",
            leader_university: "required",
            leader_country: "required",
            leader_gradyear: "required",
            leader_dateofbirth: "required",

            termsnconds: "required",
            personaldata: "required"
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function(form) {
            return true;
            //form.submit();
        }
    });
}


$(window).on( "load", function() {
    onload();

    $(document).on("submit", "#login-form", function (e) {
        e.preventDefault();
        e.stopPropagation();

        sendAjaxWithData("login", "login-form");
    });


    $(document).on("submit", "#signup-form", function (e) {
        e.preventDefault();
        e.stopPropagation();

        sendAjaxWithData("signup", "signup-form");
    });

    $(document).on("submit", "#reset-form", function (e) {
        e.preventDefault();
        e.stopPropagation();
        sendAjaxWithData("forgot", "reset-form");
        $("#login-form").delay(100).fadeIn(100);
        $("#reset-form").fadeOut(100);
    });


    $(document).on("submit", "#add-members-form", function (e) {
        e.preventDefault();
        e.stopPropagation();
        if(!confirmAddMembers()) {
            return;
        }
        sendAjaxWithData("add-members", "add-members-form");
    });

    $(document).on("click", "a.forgot", function (e) {
        e.preventDefault();
        $("#reset-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
    });

    $(document).on("click", "p.cancel", function (e) {
        e.preventDefault();
        $("#login-form").delay(100).fadeIn(100);
        $("#reset-form").fadeOut(100);
    });

    /*
    $(document).on("click", function(e) {
        console.log(e);
        console.log("this:");
        console.log(this);
        console.log("currenttarget:");
        console.log(e.currentTarget);
        console.log("delegatetarget");
        console.log(e.delegateTarget);
    });
    */

    $(document).on("click", "#login-form-link", function (e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#signup-form").fadeOut(100);
        $('#signup-form-link').removeClass('active');
        $('#signup-panelhead').removeClass('active');
        $(this).addClass('active');
        $('#login-panelhead').addClass('active');
        e.preventDefault();
    });

    $(document).on("click", "#signup-form-link", function (e) {
        $("#signup-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $("#reset-form").fadeOut(100);

        $('#login-form-link').removeClass('active');
        $('#login-panelhead').removeClass('active');
        $(this).addClass('active');
        $('#signup-panelhead').addClass('active');
        e.preventDefault();
    });


    $(document).on("click", "#addmember-link", function (e) {
        e.preventDefault();
        if (showedCoPilots >= 4) {
            return;
        }
        setRequiedAttr(true);
        $("#co-pilot" + showedCoPilots).fadeIn(100);
        $("#co-pilot" + showedCoPilots).removeClass('hidden');
        if (showedCoPilots == 2) {
            $("#remove").removeClass('hidden');
        }
        if (showedCoPilots == 3) {
            $("#add").addClass('hidden');
        }
        ++showedCoPilots;
        $("#showedCoPilots").val(showedCoPilots);
    });


    $(document).on("click", "#removemember-link", function(e) {
        e.preventDefault();
        if(showedCoPilots <= 2) {
            return;
        }
        --showedCoPilots;
        setRequiedAttr(false);
        $("#co-pilot"+showedCoPilots).fadeOut(100);
        $("#co-pilot"+showedCoPilots).addClass('hidden');
        if(showedCoPilots == 2) {
            $("#remove").addClass('hidden');
        }
        if(showedCoPilots <= 3) {
            $("#add").removeClass('hidden');
        }
        $("#showedCoPilots").val(showedCoPilots);
    });
});


function setRequiedAttr(required) {

    $('#co-pilot'+showedCoPilots+' :input').map(function() { //, #co-pilot'+showedCoPilots+' :select
        console.log($(this));

        if(required) {
            $(this).removeClass("ignore");
            //$(this).removeAttr("data-ignored");
        } else {
            $(this).addClass("ignore");
            //$(this).attr("data-ignored", "1");
        }
        //data-ignored
        formsValidates();
    });

    return;
    var k = showedCoPilots;
    $('#co-pilot'+k+' :input').map(function() { //, #co-pilot'+showedCoPilots+' :select
        console.log($(this));
        /*
        if(required) {
            $(this).attr("required", "1");
        } else {
            $(this).removeAttr("required");
        }
        //data-ignored
        */
    });
    formsValidates();
}


function loadPage(url, callback) {
    if(!callback) {
        callback = function(){};
    }

    /*
    $( "#subpage" ).load( url+".php", function( response, status, xhr ) {
        if ( status == "error" ) {
            var msg = "Sorry but there was an error: ";
            $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
        }
        callback();
    });

    return;
    */
    $.ajax({
        async: true,
        url : url,
        type : 'get',
        dataType: "html",
        error : function(data) {
            showMessage(false, "Error happened");
        },
        success : function(data) {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            $('#subpage').html(data).promise().done(function(){
                callback();
            });
        }
    });
}

function responseHandler(resp) {
    if(resp.msg) {
        showMessage(resp.success, resp.msg);
    }
    if(!resp.success) {
        return;
    }

    if(!resp.url) {
        return;
    }

    if(resp.url == "teamregistration.php" || resp.url == "upload.php") {
        $("li.dropdown").removeClass('hidden');
    }

    if(resp.url == "teamregistration.php") {
        loadPage("teamregistration.php", function() {formsValidates();});
        return;
    }

    if(resp.url == "upload.php") {
        loadPage("upload.php", function () {
            myDropzone = new Dropzone(document.getElementById("dropzone"), {
                acceptedFiles: ".pdf",
                url: "upload.php"
            });
            myDropzone.on("success", function(file) {
                let response = [];
                let success = false;
                let msg = "";
                try {
                    response = JSON.parse(file.xhr.response);
                    success = response.success;
                    msg = response.msg;
                } catch(e) {
                    unsuccessfulUpload(file);
                    return;
                }
                if(!success) {
                    unsuccessfulUpload(file, msg);
                    return;
                }
                showMessage(true, msg); //It's not error. It's a successfull message.
            });

            myDropzone.on("canceled", function(file) {
                myDropzone.removeFile(file);
            });

            myDropzone.on("error", function(file) {
                let rejected = myDropzone.getRejectedFiles();
                for(let i = 0; i < rejected.length; ++i) {
                    if(file.name == rejected[i].name) {
                        unsuccessfulUpload(file, "Unsupported file type.");
                        return;
                    }
                }
                if(file.status == "canceled") {
                    return;
                }
                unsuccessfulUpload(file);
            });

            function unsuccessfulUpload(file, error) {
                if(!error) {
                    error = "The upload was unsuccessful.";
                }
                myDropzone.removeFile(file);
                showMessage(false, error)
            }
        });
        return;
    }

    loadPage(resp.url);

}

function onload() {
    if(location.hash) {
        let hash = location.hash.substring(1);
        location.hash = "";
        hash = decodeURI(hash);
        loadPage("login.php", function() {formsValidates();showMessage(false, hash)});
        return;
        /*
        let hashArr = hash.split("|");
        if(hashArr.length == 2) {
            loadPage("login.php", function() {
                formsValidates();
                sendAjaxWithData(hashArr[0]+"&id="+hashArr[1]+"&hash="+hashArr[2]);
            });
            return;
        }
        */
    }

    loadPage("login.php", function() {formsValidates();});
    return;
    loadPage("upload.php", function () {
        myDropzone = new Dropzone(document.getElementById("dropzone"), {
            url: "upload.php"
        });
        myDropzone.on("success", function(file) {
            let response = [];
            let success = false;
            let error = "";
            try {
                response = JSON.parse(file.xhr.response);
                success = response.success;
                error = response.error;
            } catch(e) {
                unsuccessfulUpload(file);
                return;
            }
            if(!success) {
                unsuccessfulUpload(file, error);
                return;
            }
            showMessage(true, error); //It's not error. It's successfull message.
        });

        myDropzone.on("canceled", function(file) {
            myDropzone.removeFile(file);
        });

        myDropzone.on("error", function(file) {
            if(file.status == "canceled") {
                return;
            }
            unsuccessfulUpload(file);
        });

        function unsuccessfulUpload(file, error) {
            if(!error) {
                error = "The upload was unsuccessful.";
            }
            myDropzone.removeFile(file);
            showMessage(false, error)
        }
    });
}



function sendAjaxWithData(action, formID) {
    var data="";
    if(formID) {
        data = getAllFormValues('#'+formID);
    }
    $.ajax({
        async: true,
        url : "leader.php?action="+action,
        type : 'post',
        data : data,
        dataType : 'json',
        error : function(data) {
            showMessage(false, "Error happened");
        },
        success : function(data) {
            responseHandler(data);
        }
    });
}

function getAllFormValues(formName) {
    var inputValues = $(formName+' :input').map(function() {
        var name = $(this).prop("name");
        var type = $(this).prop("type");


        // checked radios/checkboxes
        if ((type == "checkbox" || type == "radio") && this.checked) {
            return name+"="+encodeURIComponent($(this).val());
        }
        // all other fields, except buttons
        else if (type != "button" || type != "submit") {
            return name+"="+encodeURIComponent($(this).val());
        }
    }).get();
    inputValues = inputValues.join('&');
    return inputValues;
}

function showMessage(success, msg) {
    if(!msg) {
        return;
    }
    $(".passwordreset").removeClass('hidden');
    $("#message").html(msg);
    setTimeout(function() {
        $(".passwordreset").addClass('hidden');
    }, "5000");
}

$(document).on('click','.navbar-collapse.show',function(e) {
    if( $(e.target).is('a') ) {
        $(this).collapse('hide');
    }
});

$( function() {
  $( "#datepicker" ).datepicker();
} );


