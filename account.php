<?php
require_once "inc/functions.php";
require_once "inc/Leader.php";

$action = isset($_GET["a"])?$_GET["a"]:"";
/*
if($action == "forgot") {
    $email = isset($_POST["email"])?$_POST["email"]:"";
    $leader = Leader::initByEmail($email);
    try {
        $success = $leader->sendForgotPassMail();
    } catch (Exception $e) {
        header("Location: /#This e-mail address is not registered yet.");
        exit();
    }
    if(!$success) {
        error_log("Couldn't send forgot pass e-mail to ".$email.".");
        header("Location: /#Something went wrong. Please try again.");
        exit();
    }
    header("Location: /#Your validation link for new password has been sent to your email. Please check your inbox.");
    exit();

} else */
if($action == "active") {
    if(!isset($_GET["id"]) || !isset($_GET["hash"])) {
        header("Location: /#Wrong link.");
        exit();
    }
    $id = $_GET["id"];
    $hash = $_GET["hash"];
    $leader = Leader::initByActivation($id, $hash);
    try {
        $leader -> update();
    } catch (Exception $e) {
        error_log("Couldn't active Leader(".$id.").\n\tHash: ".$hash."\n\tException:".$e->getMessage());
        header("Location: /#Activating profile is unsuccessful. Please try again.");
        exit();
    }
    header("Location: /#Profile activated. Please log in.");
    exit();
} else if($action == "reset") {


    if(!isset($_GET["id"]) || !isset($_GET["hash"])) {
        header("Location: /#Wrong link.");
        exit();
    }
    $id = $_GET["id"];
    $hash = $_GET["hash"];
    $leader = Leader::initByResetHash($id, $hash);
    $newPass = "Wizz".rand(1039,9999);
    $leader -> setPass($newPass);
    try {
        $leader -> update();
        $leader -> sendNewPassInMail($newPass);
    } catch (Exception $e) {
        error_log("Couldn't save the Leader(".$id.") updates on resetting password.\n\tHash: ".$hash."\n\tException:".$e->getMessage());
        header("Location: /#Resetting password is unsuccessful. Please try again.");
        exit();
    }
    header("Location: /#Your new password has been sent to your email. Please check your inbox.");
    exit();
} else if($action == "logout") {
    $leader = Leader::initByToken();
    $leader -> logout();
    header("Location: /#You have been logged out.");
    exit();
} else if($action == "delete") {
    $leader = Leader::initByToken();
    if(!$leader->isLogged()) {
        header("Location: /#Please log in to delete your account.");
        exit();
    }
    $teamID = "unknown id";
    $leaderID = "unknown id";
    try {
        $teamID = $leader -> getTeam() -> getId();
        $leaderID = $leader -> getID();
        $leader -> getTeam() -> del();
        $leader -> logout();
    } catch (Exception $e) {
        error_log("Couldn't delete the Leader(".$leaderID.") and Team(".$teamID.").\n\tException:".$e->getMessage());
        header("Location: /#Error on deleting your data. Please try again or contact us.");
        exit();
    }
    header("Location: /#Your account has been deleted.");
    exit();
} else if($action == "resend") {
    $leader = null;
    $success = true;
    $leaderID = 0;
    try {
        $leader = Leader::initByToken();
        if(!$leader -> isLogged()) {
            header("Location: /#You are not logged in.");
            exit();
        } else if($leader -> isActived()) {
            header("Location: /#You have already activated your profile.");
            exit();
        }
        $leader -> sendConfirmMail();
        $leaderID = $leader->getID();
    } catch (Exception $e) {
        error_log("Couldn't resend activate e-mail to Leader(".$leaderID.").\n\tException:".$e->getMessage());
        $success = false;
    }

    if($success) {
        header("Location: /#The link has been sent to your e-mail.");
        exit();
    } else {
        header("Location: /#Some error accrued. Please try again.");
        exit();
    }
}
header("Location: /#Wrong link.");
