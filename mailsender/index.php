<?php
require_once "../inc/functions.php";
require_once 'Mail.php';
require_once 'Mail/mime.php';

$success = false;
$email = "";
$contect = "";
$subject = "";
$errorMsg = "";


if(isset($_POST["content"]) && isset($_POST["email"]) && isset($_POST["subject"])) {
    $subject = $_POST["subject"];
    $email = $_POST["email"];
    $content = $_POST["content"];

        if (!sendMail($email, $subject, $content, true)) {
            $errorMsg .= "Nem ment ki az e-mail a(z) " . $email . " címre.";
        }

        if (!sendMail(MAIL_SENDER, $subject, $content, true)) {
            $errorMsg .= "Nem ment ki az e-mail a(z) " . MAIL_SENDER . " címre.<br />";
        }

    if($errorMsg == "") {
        header("Location: ./?ok=1");
        exit();
    }

}
?>

<html>
<head>
    <title>E-mail küldő</title>
</head>
<body>
<?php
if(isset($_GET["ok"])) {
    exit('E-mail elküldve. Át leszel irányítva. <meta http-equiv="refresh" content="5; URL=\'/\'" />');
}
?>
    <h1><?=$errorMsg==""?"":"HIBA: ".$errorMsg;?></h1>
    <h2>Ebből menni fog egy másolat az <?=MAIL_SENDER?> címre is.</h2>
    <form action="" method="post">
        <input placeholder="email cím" type="email" name="email" value="<?=$email?>"><br />
        <input placeholder="tárgy" type="text" name="subject" value="<?=$subject?>"><br />
        <textarea placeholder="szöveg" name="content"><?=$contect?></textarea><br />
        <input type="submit" value="küldés">
    </form>
</body>
</html>
