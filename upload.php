<?php
require_once "inc/functions.php";
require_once "inc/Leader.php";
require_once "inc/Uploader.php";
$error = false;
$msg = "";

$date = DateTime::createFromFormat('d/m/Y H:i:s', LUNCH_TIME);
$d = $date->getTimestamp();
if(strtotime("now") < $d) {
    $error = true;
    $msg = 'You can submit your solution here, after the case release. Please do not forget to send us a paper version of the <a href="frontend/assets/files/reg-form.pdf" target="_blank">registration form</a> in order to finalize your application. The case study will be sent to you on 26 March. Until then, stay tuned to Wizz Air’s social media channels for any updates.';
}

$deadlineDate = DateTime::createFromFormat('d/m/Y H:i:s', DEADLINE);
$deadlineD = $deadlineDate->getTimestamp();
if(strtotime("now") > $deadlineD) {
    $error = true;
    $msg = 'The submission period is over, thank you for your participation';
}

    $leader = null;
if(!$error) {
    $leader = Leader::initByToken();
    if (!$leader->isLogged()) {
        $error = true;
        $msg = "You are not logged in.";
    } else if (!$leader->isActived()) {
        $error = true;
        $msg = 'You have to activate your account before submission. Check your inbox for the link<br><a href="./account.php?a=resend">Click here to resend it</a>';
    } else if (!$leader->getTeam()->canUpload()) {
        $error = true;
        $msg = "You've already uploaded your solution.";
    }
}

if (!empty($_FILES)) {

    if ($error) {
        response(false, $msg);
    }

    $teamName = $leader->getTeam()->getName();
    $upload = new Uploader(true, ALLOWED_TYPES, UPLOAD_PATH . $teamName, 0, 10, 0, 0);

    $upload->singleFile($_FILES["file"]);
    $upload->saveFiles();
    if ($upload->isSuccessful() && $leader->getTeam()->isFirstUpload()) {
        $leader->getTeam()->setUploadDate(date("Y-m-d H:i:s"));
        $leader->getTeam()->update();
    }

    response($upload->isSuccessful(), !$upload->isSuccessful()?$msg:'Thank you for your submission. We will soon be in touch');
}

?>
<div class="errmsg <?=(!$error?'hidden':'');?>"><?=$msg;?></div>

<div id="uploader" role="main">
    <form id="dropzone" action="upload.php" enctype="multipart/form-data" class="dropzone">
        <div class="fallback">
            <input name="file" type="file" />
        </div>
    </form>
</div>